import 'package:flutter/foundation.dart';
import 'package:mini_project/services/services.dart';
import 'package:mini_project/utils/state/finite_state.dart';

import '../models/delete_models.dart';

class DeleteItemProvider extends ChangeNotifier {
  late DeleteModels _deleteResult;
  late String _errorMessage;
  final ApiServices service = ApiServices();

  DeleteModels get deleteResult => _deleteResult;
  String get errorMessage => _errorMessage;
  MyState myState = MyState.initial;
  Future<void> deleteItem(int id) async {
    try {
      myState = MyState.loaded;
      _deleteResult = await ApiServices().deleteItem(id);
      _errorMessage = '';
      notifyListeners();
    } catch (e) {
      myState = MyState.failed;
      _deleteResult = DeleteModels(status: 0, msg: '');
      _errorMessage = 'Failed to delete item: $e';
      notifyListeners();
    }
  }
}
