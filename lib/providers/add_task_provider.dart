import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:mini_project/models/add_task_model.dart';
import 'package:mini_project/models/add_todo_done_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../models/edit_task_model.dart';
import '../services/services.dart';
import '../services/shared_services.dart';
import '../utils/constant.dart';
import '../utils/state/finite_state.dart';

class AddTaskProvider extends ChangeNotifier {
  AddTaskModel? _addTaskModel;
  MyState myState = MyState.initial;

  Future addTask({
    required String title,
    required String description,
    required String finsihAt,
  }) async {
    try {
      final loginData = await SharedPreferences.getInstance();
      myState = MyState.loading;
      notifyListeners();

      final result = await ApiServices.addTask(
        title: title,
        description: description,
        finsihAt: finsihAt,
        token: loginData.getString('login') ?? '',
      );

      if (result != null) {
        _addTaskModel = result;
        myState = MyState.loaded;
        notifyListeners();
      } else {
        myState = MyState.failed;
        notifyListeners();
      }
    } catch (e) {
      if (kDebugMode) {
        var statusCode;
        print(e.toString());
      }

      myState = MyState.failed;
      notifyListeners();
    }
  }
}
