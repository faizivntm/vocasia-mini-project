import 'package:flutter/material.dart';
import 'package:mini_project/services/services.dart';

import '../models/todo_overdue_model.dart';

class TodoOverdueProvider extends ChangeNotifier {
  TodoOverdueModel? _todoOverdue;
  final ApiServices _todoOverdueService = ApiServices();

  TodoOverdueModel? get todoOverdue => _todoOverdue;
  Future<void> fetchTodoOverdue() async {
    try {
      final result = await _todoOverdueService.fetchTodoOverdue();
      _todoOverdue = result;
      notifyListeners();
    } catch (e) {
      throw e;
    }
  }
}
