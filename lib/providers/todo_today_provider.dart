import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:mini_project/models/todo_today_model.dart';

import '../services/services.dart';
import '../utils/state/finite_state.dart';

class TodoTodayProvider extends ChangeNotifier {
  final ApiServices service = ApiServices();

  TodoTodayModel? todoTodayModel;

  MyState myState = MyState.initial;
  Future fetchTodoToday() async {
    try {
      myState = MyState.loading;
      notifyListeners();

      todoTodayModel = await service.fetchTodoToday();

      myState = MyState.loaded;
      notifyListeners();
    } catch (e) {
      if (e is DioError) {
        if (kDebugMode) {
          print(e.response!.statusCode);
          print(e.response!.statusMessage);
          print(e.response!.data);
        }
      }

      myState = MyState.failed;
      notifyListeners();
    }
  }
}
