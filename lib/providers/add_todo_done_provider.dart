import 'package:flutter/foundation.dart';
import 'package:mini_project/models/add_todo_done_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../services/services.dart';
import '../services/shared_services.dart';
import '../utils/constant.dart';
import '../utils/state/finite_state.dart';

class AddTodoDoneProvider extends ChangeNotifier {
  late AddTodoDoneModel _addDoneResult;
  final ApiServices service = ApiServices();

  AddTodoDoneModel get doneResult => _addDoneResult;
  MyState myState = MyState.initial;
  Future<void> addTodoDone(int id) async {
    try {
      myState = MyState.loaded;
      _addDoneResult = await ApiServices().addTodoDone(id);
      notifyListeners();
    } catch (e) {
      myState = MyState.failed;
      _addDoneResult = AddTodoDoneModel(id: id);
      notifyListeners();
    }
  }
}
