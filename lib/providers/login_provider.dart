import 'package:flutter/foundation.dart';
import 'package:mini_project/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../services/services.dart';
import '../services/shared_services.dart';
import '../utils/state/finite_state.dart';
import 'package:http/http.dart' as http;

class LogInProvider extends ChangeNotifier {
  final ApiServices service = ApiServices();
  late SharedPreferences loginData;
  final prf = SharedService();
  LogInProvider? users;

  MyState myState = MyState.initial;
  Future signIn({
    required String email,
    required String password,
  }) async {
    try {
      myState = MyState.loading;
      notifyListeners();
      loginData = await SharedPreferences.getInstance();

      final users = await service.signIn(
        email: email,
        password: password,
      );

      final response = await http.post(
        Uri.parse('${Constant.linkApi}login'),
        body: {'email': email, 'password': password},
      );

      if (response.statusCode == 200) {
        prf.saveToken(users.accessToken!);
        await loginData.setString('login', users.accessToken!);
        myState = MyState.loaded;
        notifyListeners();
      } else {
        myState = MyState.failed;
        notifyListeners();
      }
    } catch (e) {
      if (kDebugMode) {
        print(e.toString());
      }

      myState = MyState.failed;
      notifyListeners();
    }
  }
}
