import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:mini_project/models/add_task_model.dart';
import 'package:mini_project/models/add_todo_done_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import '../models/edit_task_model.dart';
import '../services/services.dart';
import '../services/shared_services.dart';
import '../utils/constant.dart';
import '../utils/state/finite_state.dart';

class EditTaskProvider extends ChangeNotifier {
  EditTaskModel? _editTaskModel;
  MyState myState = MyState.initial;

  Future ediTask({
    required int id,
    required String title,
    required String description,
  }) async {
    try {
      final loginData = await SharedPreferences.getInstance();
      myState = MyState.loading;
      notifyListeners();

      final response = await http.post(
        Uri.parse(
          '${Constant.linkApi}todo/editingToday',
        ),
        headers: {"Authorization": "Bearer ${loginData.getString('login')}"},
        body: {
          'id_todos': id.toString(),
          'title': title,
          'description': description,
        },
      );

      if (response.statusCode == 200) {
        _editTaskModel = EditTaskModel.fromJson(jsonDecode(response.body));
        myState = MyState.loaded;
        notifyListeners();
      } else {
        myState = MyState.failed;
        notifyListeners();
      }
    } catch (e) {
      if (kDebugMode) {
        var statusCode;
        print(e.toString());
      }

      myState = MyState.failed;
      notifyListeners();
    }
  }
}
