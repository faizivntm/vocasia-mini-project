import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../models/notif_model.dart';
import '../services/services.dart';
import '../services/shared_services.dart';
import '../utils/state/finite_state.dart';

class NotifProvider extends ChangeNotifier {
  final ApiServices service = ApiServices();
  late SharedPreferences notifData;
  final prf = SharedService();
  NotifModel? notifModel;

  MyState myState = MyState.initial;
  Future getNotifData() async {
    try {
      myState = MyState.loading;
      notifyListeners();
      notifModel = await service.getNotifData();
      myState = MyState.loaded;
      notifyListeners();
    } catch (e) {
      if (e is DioError) {
        if (kDebugMode) {
          print(e.response!.statusCode);
          print(e.response!.statusMessage);
          print(e.response!.data);
        }
      }
    }
    myState = MyState.failed;
    notifyListeners();
    // return null;
  }
}
