import 'dart:convert';
import 'package:flutter/foundation.dart';
import '../models/signup_models.dart';
import '../utils/constant.dart';
import '../utils/state/finite_state.dart';
import 'package:http/http.dart' as http;

class SignUpProvider extends ChangeNotifier {
  SignUpModel? users;
  MyState myState = MyState.initial;

  Future signUp(
      {required String email,
      required String username,
      required String password,
      required String password_confirm}) async {
    try {
      myState = MyState.loading;
      notifyListeners();

      final response = await http.post(
        Uri.parse('${Constant.linkApi}register'),
        body: {
          'email': email,
          'username': username,
          'password': password,
          'password_confirm': password_confirm
        },
      );

      if (response.statusCode == 200) {
        users = SignUpModel.fromJson(jsonDecode(response.body));
        myState = MyState.loaded;
        notifyListeners();
      } else {
        myState = MyState.failed;
        notifyListeners();
      }
    } catch (e) {
      if (kDebugMode) {
        var statusCode;
        print(e.toString());
      }

      myState = MyState.failed;
      notifyListeners();
    }
  }
}
