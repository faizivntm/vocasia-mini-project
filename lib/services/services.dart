import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:mini_project/models/add_task_model.dart';
import 'package:mini_project/models/add_todo_done_model.dart';
import 'package:mini_project/models/delete_models.dart';
import 'package:mini_project/models/edit_task_model.dart';
import 'package:mini_project/utils/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:mini_project/models/count_todo_today_model.dart';
import 'package:mini_project/models/todo_today_model.dart';
import '../models/todo_done_model.dart';
import '../models/login_models.dart';
import '../models/notif_model.dart';
import '../models/signup_models.dart';
import 'package:http/http.dart' as http;

import '../models/todo_overdue_model.dart';
import '../models/user_model.dart';

class ApiServices {
  Dio dio = Dio();

  Future<SignUpModel> signUp({
    required String email,
    required String username,
    required String password,
    required String password_confirm,
  }) async {
    try {
      final response = await http.post(
        Uri.parse('${Constant.linkApi}register'),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: <String, String>{
          'email': email,
          'username': username,
          'password': password,
          'password_confirm': password_confirm,
        },
      );
      print(response.body);
      return SignUpModel.fromJson(jsonDecode(response.body));
    } catch (e) {
      rethrow;
    }
  }

  Future<LogInModel> signIn({
    required String email,
    required String password,
  }) async {
    try {
      final response = await http.post(
        Uri.parse('${Constant.linkApi}login'),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: <String, String>{
          'email': email,
          'password': password,
        },
      );
      print(response.body);
      return LogInModel.fromJson(jsonDecode(response.body));
    } catch (e) {
      rethrow;
    }
  }

  Future<AddTodoDoneModel> addTodoDone(int id) async {
    try {
      final loginData = await SharedPreferences.getInstance();
      final response = await dio.post(
        '${Constant.linkApi}todo/addTodoDone?id_todos=$id',
        options: Options(
          headers: {"Authorization": "Bearer ${loginData.getString('login')}"},
        ),
      );
      if (response.statusCode == 200) {
        return AddTodoDoneModel.fromJson(response.data);
      } else {
        throw Exception('Failed to delete item.');
      }
    } catch (e) {
      throw Exception('Failed to delete item: $e');
    }
  }

  Future<EditTaskModel> editTask({
    required int id,
    required String title,
    required String description,
  }) async {
    try {
      final loginData = await SharedPreferences.getInstance();
      final response = await http.post(
        Uri.parse('${Constant.linkApi}todo/editingToday'),
        headers: <String, String>{
          "Authorization": "Bearer ${loginData.getString('login')}",
        },
        body: <String, String>{
          'id_todos': id.toString(),
          'title': title,
          'description': description,
        },
      );
      print(response.body);
      return EditTaskModel.fromJson(jsonDecode(response.body));
    } catch (e) {
      rethrow;
    }
  }

  Future<DeleteModels> deleteItem(int id) async {
    try {
      final loginData = await SharedPreferences.getInstance();
      final response = await dio.delete(
        '${Constant.linkApi}todo/deleteTodo/$id',
        options: Options(
          headers: {"Authorization": "Bearer ${loginData.getString('login')}"},
        ),
      );
      if (response.statusCode == 200) {
        return DeleteModels.fromJson(response.data);
      } else {
        throw Exception('Failed to delete item.');
      }
    } catch (e) {
      throw Exception('Failed to delete item: $e');
    }
  }

  Future getNotifData() async {
    try {
      final loginData = await SharedPreferences.getInstance();
      final response = await http.get(
        Uri.parse(
          '${Constant.linkApi}todo/getTodoOverdue',
        ),
        headers: {"Authorization": "Bearer ${loginData.getString('login')}"},
      );
      if (response.statusCode == 200) {
        print(response.body);
        final Map<String, dynamic> responseJson = jsonDecode(response.body);
        final List<dynamic> responseData = responseJson['data'];
        final List<NotifModel> data =
            responseData.map((json) => NotifModel.fromJson(json)).toList();
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }

  Future<UserModel> fetchUsers() async {
    final loginData = await SharedPreferences.getInstance();
    try {
      final response = await http.get(
        Uri.parse('${Constant.linkApi}user/getUser'),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': 'Bearer ${loginData.getString('login')}',
        },
      );
      print(response.body);

      return UserModel.fromJson(jsonDecode(response.body));
    } catch (e) {
      rethrow;
    }
  }

  Future fetchTodoToday() async {
    try {
      final loginData = await SharedPreferences.getInstance();
      final response = await http.get(
        Uri.parse('${Constant.linkApi}todo/todoToday'),
        headers: {"Authorization": "Bearer ${loginData.getString('login')}"},
      );

      if (response.statusCode == 200) {
        print(response.body);
        final Map<String, dynamic> responseJson = jsonDecode(response.body);
        final List<dynamic> responseData = responseJson['data'];
        final List<TodoTodayModel> data =
            responseData.map((json) => TodoTodayModel.fromJson(json)).toList();
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }

  Future fetchTodoOverdue() async {
    try {
      final loginData = await SharedPreferences.getInstance();
      final response = await http.get(
        Uri.parse('${Constant.linkApi}todo/getTodoOverdue'),
        headers: {"Authorization": "Bearer ${loginData.getString('login')}"},
      );

      if (response.statusCode == 200) {
        print(response.body);
        final Map<String, dynamic> responseJson = jsonDecode(response.body);
        final List<dynamic> responseData = responseJson['data'];
        final List<TodoOverdueModel> data = responseData
            .map((json) => TodoOverdueModel.fromJson(json))
            .toList();
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }

  Future fetchTodoDone() async {
    try {
      final loginData = await SharedPreferences.getInstance();
      final response = await http.get(
        Uri.parse('${Constant.linkApi}todo/getTodoDone'),
        headers: {"Authorization": "Bearer ${loginData.getString('login')}"},
      );

      if (response.statusCode == 200) {
        print(response.body);
        final Map<String, dynamic> responseJson = jsonDecode(response.body);
        final List<dynamic> responseData = responseJson['data'];
        final List<TodoDoneModel> data =
            responseData.map((json) => TodoDoneModel.fromJson(json)).toList();
        return data;
      }
    } catch (e) {
      print(e.toString());
    }
    return [];
  }

  Future<CountTodoTodayModel> fetchCountTodoToday() async {
    final response =
        await http.get(Uri.parse('${Constant.linkApi}todo/countTodoToday'));

    if (response.statusCode == 200) {
      return CountTodoTodayModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load task today');
    }
  }

  static Future<AddTaskModel?> addTask({
    required String title,
    required String description,
    required String finsihAt,
    required String token,
  }) async {
    try {
      final response = await http.post(
        Uri.parse('${Constant.linkApi}todo/createTodo'),
        headers: {"Authorization": "Bearer $token"},
        body: {
          'title': title,
          'description': description,
          'finish_at': finsihAt,
        },
      );

      if (response.statusCode == 200) {
        return AddTaskModel.fromJson(jsonDecode(response.body));
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }
}
