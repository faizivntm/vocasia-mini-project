class Constant {
  static const double fontExtraSmall = 10.0;
  static const double fontSmall = 12.0;
  static const double fontSemiSmall = 14.0;
  static const double fontRegular = 15.0;
  static const double fontSemiRegular = 16.0;
  static const double fontSemiBig = 18.0;
  static const double fontBig = 22.0;
  static const double fontExtraBig = 28.0;
  static const double fontTitle = 24.0;

  static const int colorTextDateGrey = 0xFF737373;
  static const int colorGreyCCCCCC = 0xFFCCCCCC;
  static const int colorWhiteFBFBFB = 0xFFFBFBFB;
  static const int colorGreen1BA345 = 0xFF1BA345;
  static const int colorRedBA181B = 0xFFBA181B;
  static const int colorWhiteFFFFFF = 0xFFFFFFFF;
  static const int colorGrey737373 = 0xFF737373;

  static const String linkApi = 'https://ilhampradana-miniproject.my.id/';
}
