import 'package:flutter/material.dart';
import 'package:mini_project/providers/add_task_provider.dart';
import 'package:mini_project/providers/add_todo_done_provider.dart';
import 'package:mini_project/providers/delete_provider.dart';
import 'package:mini_project/providers/login_provider.dart';
import 'package:mini_project/providers/signup_provider.dart';
import 'package:mini_project/views/home_screen/view/home_screen.dart';
import 'package:mini_project/views/login_screen/views/login_screen.dart';
import 'package:mini_project/views/notification_screen/views/notification_screen.dart';
import 'package:mini_project/providers/todo_overdue_provider.dart';
import 'package:mini_project/providers/todo_today_provider.dart';
import 'package:mini_project/providers/user_provider.dart';
import 'package:mini_project/views/register_screen/views/register_screen.dart';
import 'package:provider/provider.dart';
import 'providers/edit_task_provider.dart';
import 'providers/notif_provider.dart';
import 'views/add_task_screen/views/add_task_screen.dart';
import 'views/splash_screen/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(
            create: (context) => LogInProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => SignUpProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => NotifProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => DeleteItemProvider(),
          ),
          ChangeNotifierProvider(
            create: (context) => NotifProvider(),
          ),
          ChangeNotifierProvider(create: (_) => UserProvider()),
          ChangeNotifierProvider(create: (_) => TodoTodayProvider()),
          ChangeNotifierProvider(create: (_) => TodoOverdueProvider()),
          ChangeNotifierProvider(create: (_) => AddTaskProvider()),
          ChangeNotifierProvider(create: (_) => EditTaskProvider()),
          ChangeNotifierProvider(create: (_) => AddTodoDoneProvider()),
          ChangeNotifierProvider(create: (_) => LogInProvider()),
        ],
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              scaffoldBackgroundColor: Colors.white,
              fontFamily: 'Poppins',
            ),
            home: const SplashScreen(),
            routes: {
              SplashScreen.routeName: (context) => const SplashScreen(),
              LoginScreen.routeName: (context) => const LoginScreen(),
              RegisterScreen.routeName: (context) => const RegisterScreen(),
              HomeScreen.routeName: (context) => const HomeScreen(),
              NotifScreen.routeName: (context) => NotifScreen(),
              AddTaskScreen.routeName: (context) => AddTaskScreen()
            }));
  }
}
