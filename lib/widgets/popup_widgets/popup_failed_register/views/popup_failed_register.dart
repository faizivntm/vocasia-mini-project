import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:mini_project/widgets/popup_widgets/popup_failed_register/widgets/button_try_again.dart';

import '../../../../views/register_screen/views/register_screen.dart';

class FailedRegister extends StatefulWidget {
  const FailedRegister({super.key});

  @override
  State<FailedRegister> createState() => _FailedRegisterState();
}

class _FailedRegisterState extends State<FailedRegister> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.314,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: size.height * 0.120,
              child: Lottie.asset(
                "assets/animate/red-cross.json",
                height: size.height * 0.250,
                width: size.width * 0.250,
              ),
            ),
            Container(
              child: Text("Registration Failed"),
            ),
            SizedBox(
              height: size.height * 0.015,
            ),
            Container(
              child: Text(
                "Please complete the data and Fill the data correctly. ",
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: size.height * 0.030,
            ),
            ButtonTryAgain(onPressed: () => Navigator.pop(context)),
          ],
        ));
  }
}
