import 'package:flutter/material.dart';

class DeleteButtonYes extends StatelessWidget {
  const DeleteButtonYes({super.key, required this.onPressed});
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        child: Text('Yes'),
        style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFFBA181C),
            minimumSize: Size(74, 36),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            )));
  }
}
