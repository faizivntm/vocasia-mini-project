import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:mini_project/providers/delete_provider.dart';
import 'package:mini_project/utils/state/finite_state.dart';
import 'package:mini_project/views/home_screen/widget/tab_bar_widgets/tab_bar_controller_widgets/tab_bar_controller_todo_today_widget.dart';
import 'package:mini_project/views/home_screen/widget/tab_bar_widgets/tab_bar_view_widgets/tab_bar_view_done_widget.dart';
import 'package:mini_project/views/home_screen/widget/tab_bar_widgets/tab_bar_view_widgets/tab_bar_view_overdue_widget.dart';
import 'package:mini_project/views/home_screen/widget/tab_bar_widgets/tab_bar_view_widgets/tab_bar_view_to_do_widget.dart';
import 'package:provider/provider.dart';
import '../../../../views/home_screen/view/home_screen.dart';
import '../widgets/delete_button_no.dart';
import '../widgets/delete_button_yes.dart';

class DeleteTask extends StatefulWidget {
  final int id;
  final String todo;
  final String todoOverdue;
  final String todoDone;
  const DeleteTask({
    Key? key,
    required this.id,
    required this.todo,
    required this.todoOverdue,
    required this.todoDone,
  }) : super(key: key);
  @override
  State<DeleteTask> createState() => _DeleteTaskState();
}

class _DeleteTaskState extends State<DeleteTask> {
  @override
  void initState() {
    final provider = Provider.of<DeleteItemProvider>(context, listen: false);
    provider.addListener(
      () {
        if (provider.myState == MyState.failed) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 5),
              content: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text("Gagal Delete!"),
              ),
              backgroundColor: Colors.red,
            ),
          );
        } else if (provider.myState == MyState.loaded) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 5),
              content: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text("Delete Berhasil!"),
              ),
              backgroundColor: Colors.green,
            ),
          );
          String todoOverdue = "todoOverdue";
          String todo = "todo";
          String todoDone = "todoDone";
          if (todo == widget.todo) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(
                  selectedIndex: 0,
                  //indeks tab yang ingin ditampilkan
                ),
              ),
            );
          } else if (todoOverdue == widget.todoOverdue) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(
                  selectedIndex: 1,
                  //indeks tab yang ingin ditampilkan
                ),
              ),
            );
          } else if (todoDone == widget.todoDone) {
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => HomeScreen(
                  selectedIndex: 2,
                  //indeks tab yang ingin ditampilkan
                ),
              ),
            );
          }
        }
      },
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<DeleteItemProvider>(context, listen: false);
    final size = MediaQuery.of(context).size;
    String id_todos = "";

    return Container(
        height: size.height * 0.175,
        child: Column(
          children: [
            Container(
              child: Row(
                children: [
                  Container(
                    child: Lottie.asset(
                      "assets/animate/alert.json",
                      width: size.width * 0.250,
                      height: size.height * 0.110,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Container(
                    width: size.width * 0.35,
                    child: Text(
                      "Are you sure you want to delete this task?",
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  DeleteButtonNo(onPressed: () => Navigator.pop(context)),
                  DeleteButtonYes(
                    onPressed: () async {
                      await provider.deleteItem(widget.id);
                    },
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
