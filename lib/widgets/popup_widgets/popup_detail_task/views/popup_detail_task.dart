import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mini_project/views/edit_task_screen/views/edit_task_screen.dart';
import 'package:mini_project/widgets/popup_widgets/popup_detail_task/widgets/button_delete.dart';
import 'package:mini_project/widgets/popup_widgets/popup_detail_task/widgets/button_edit_task.dart';

import '../../popup_delete_tatsk/views/popup_delete_task.dart';

class DetailTask extends StatefulWidget {
  final String date;
  final String clock;
  final String title;
  final String todo;
  final String todoOverdue;
  final int id;
  final String todoDone;
  const DetailTask({
    Key? key,
    required this.date,
    required this.clock,
    required this.title,
    required this.id,
    required this.todo,
    required this.todoOverdue,
    required this.todoDone,
  }) : super(key: key);
  @override
  State<DetailTask> createState() => _DetailTaskState();
}

class _DetailTaskState extends State<DetailTask> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.all(40),
      child: SingleChildScrollView(
          child: Column(
        children: [
          Container(child: Image.asset("assets/icons/line_swipe.png")),
          SizedBox(
            height: size.height * 0.036,
          ),
          Container(
            child: Text(
              widget.title,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          SizedBox(
            height: size.height * 0.036,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Container(
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Color(0xFF1BA345),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  height: 23,
                  width: 97,
                  child: Text(
                    "Daily Activity",
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  ),
                ),
                SizedBox(
                  width: size.width * 0.02,
                ),
                Container(
                  alignment: Alignment.center,
                  height: 23,
                  width: 97,
                  decoration: BoxDecoration(
                      color: Color(0xFF458FF6),
                      borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    "No Repetition",
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  ),
                ),
                SizedBox(
                  width: size.width * 0.02,
                ),
                Container(
                  height: 23,
                  width: 97,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Color(0xFFFEC001),
                      borderRadius: BorderRadius.circular(10)),
                  child: Text(
                    "Notification On",
                    style: TextStyle(fontSize: 10, color: Colors.white),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: size.height * 0.034,
          ),
          Container(
            padding: EdgeInsets.only(left: 30, right: 30),
            child: Row(
              children: [
                Icon(Icons.calendar_month),
                SizedBox(
                  width: size.width * 0.005,
                ),
                Text("date"),
                Spacer(),
                Text(widget.date)
              ],
            ),
          ),
          SizedBox(
            height: size.height * 0.020,
          ),
          Container(
            padding: EdgeInsets.only(left: 30, right: 30),
            child: Row(
              children: [
                Icon(
                  Icons.timer,
                ),
                SizedBox(
                  width: size.width * 0.005,
                ),
                Text("Time"),
                Spacer(),
                Text(widget.clock)
              ],
            ),
          ),
          SizedBox(
            height: 40,
          ),
          Container(
              height: size.height * 0.055,
              width: size.width * 0.815,
              child: ButtonEdit(onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditTaskScreen(
                        id: widget.id,
                        date: widget.date,
                        clock: widget.clock,
                        todo: widget.todo,
                        todoOverdue: widget.todoOverdue,
                        todoDone: widget.todoDone,
                      ),
                    ));
              })),
          SizedBox(
            height: 14,
          ),
          Container(
              height: size.height * 0.055,
              width: size.width * 0.815,
              child: ButtonDelete(
                onPressed: () {
                  AlertDialog alert = AlertDialog(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25),
                    ),
                    content: DeleteTask(
                      id: widget.id,
                      todo: widget.todo,
                      todoOverdue: widget.todoOverdue,
                      todoDone: widget.todoDone,
                    ),
                  );

                  showDialog(context: context, builder: (context) => alert);
                  return;
                },
              )),
        ],
      )),
    );
  }
}
