import 'package:flutter/material.dart';

class ButtonDelete extends StatelessWidget {
  const ButtonDelete({super.key, required this.onPressed});
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        child: Text('Delete'),
        style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFFCCCCCC),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            )));
  }
}
