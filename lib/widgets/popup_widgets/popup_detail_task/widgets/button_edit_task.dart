import 'package:flutter/material.dart';

class ButtonEdit extends StatelessWidget {
  const ButtonEdit({super.key, required this.onPressed});
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        child: Text('Edit'),
        style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFFBA181C),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            )));
  }
}
