import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:mini_project/widgets/popup_widgets/popup_success_register/widgets/button_login.dart';

import '../../../../views/login_screen/views/login_screen.dart';

class SuccessRegister extends StatefulWidget {
  static const String routeName = 'popup-success-screen';
  const SuccessRegister({super.key});

  @override
  State<SuccessRegister> createState() => _SuccessRegisterState();
}

class _SuccessRegisterState extends State<SuccessRegister> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
        height: size.height * 0.500,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: size.height * 0.20,
              child: Lottie.asset(
                "assets/animate/checked.json",
                height: size.height * 100,
                width: size.width * 100,
              ),
            ),
            Container(
              child: Text(
                "Your Account Was Created",
              ),
            ),
            SizedBox(
              height: size.height * 0.0001,
            ),
            Container(
              child: Text(
                "Congratulations! You just created an account. Now you can log in or save password for later.",
                textAlign: TextAlign.center,
              ),
            ),
            SizedBox(
              height: size.height * 0.0001,
            ),
            ButtonLogin(
                onPressed: () => Navigator.pushReplacementNamed(
                    context, LoginScreen.routeName)),
          ],
        ));
  }
}
