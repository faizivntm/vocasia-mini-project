import 'package:flutter/material.dart';

class ButtonLogin extends StatelessWidget {
  const ButtonLogin({super.key, required this.onPressed});
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ElevatedButton(
      onPressed: onPressed,
      child: Text('Login'),
      style: ElevatedButton.styleFrom(
          backgroundColor: Color(0xFFBA181C),
          minimumSize: Size(292, 42),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          )),
    ));
  }
}
