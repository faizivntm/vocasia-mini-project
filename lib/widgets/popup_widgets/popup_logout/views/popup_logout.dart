import "package:flutter/material.dart";
import 'package:lottie/lottie.dart';
import 'package:mini_project/widgets/popup_widgets/popup_logout/widgets/logout_button_no.dart';

import '../../../../services/shared_services.dart';
import '../../../../views/login_screen/views/login_screen.dart';
import '../widgets/logout_button_yes.dart';

class PopupLogout extends StatefulWidget {
  const PopupLogout({super.key});

  @override
  State<PopupLogout> createState() => _PopupLogoutState();
}

class _PopupLogoutState extends State<PopupLogout> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      child: Image.asset(
        'assets/images/image_profile.png',
        scale: 12.0,
      ),
      onTap: () {
        AlertDialog alert = AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(25),
          ),
          content: SingleChildScrollView(
              child: Column(
            children: [
              Row(
                children: [
                  Container(
                    child: Lottie.asset(
                      "assets/animate/alert.json",
                      width: size.width * 0.250,
                      height: size.height * 0.100,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Container(
                    width: size.width * 0.35,
                    child: Text(
                      "Are you sure you want to logout?",
                      style: TextStyle(fontSize: 15),
                      textAlign: TextAlign.justify,
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  LogoutButtonNo(onPressed: () => Navigator.pop(context)),
                  LogoutButtonYes(
                    onPressed: () {
                      final prf = SharedService();
                      prf.deleteToken();
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          duration: Duration(seconds: 1),
                          content: Text(
                            'Berhasil Keluar',
                          )));
                      Navigator.pushNamedAndRemoveUntil(
                          context, LoginScreen.routeName, (route) => false);
                    },
                  ),
                ],
              ),
            ],
          )),
        );

        showDialog(context: context, builder: (context) => alert);
        return;
      },
    );
  }
}
