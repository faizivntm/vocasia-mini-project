import 'package:flutter/material.dart';

class LogoutButtonYes extends StatelessWidget {
  const LogoutButtonYes({super.key, required this.onPressed});
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        child: Text('Yes'),
        style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFFBA181C),
            minimumSize: Size(74, 36),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            )));
  }
}
