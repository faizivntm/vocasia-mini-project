import 'package:flutter/material.dart';

class LogoutButtonNo extends StatelessWidget {
  const LogoutButtonNo({super.key, required this.onPressed});
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        child: Text('No'),
        style: ElevatedButton.styleFrom(
            backgroundColor: Color(0xFFCCCCCC),
            minimumSize: Size(74, 36),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            )));
  }
}
