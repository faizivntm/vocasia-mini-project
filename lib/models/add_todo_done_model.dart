class AddTodoDoneModel {
  final int id;

  AddTodoDoneModel({required this.id});

  factory AddTodoDoneModel.fromJson(Map<String, dynamic> json) {
    return AddTodoDoneModel(
      id: json['id_todos'] as int,
    );
  }
}
