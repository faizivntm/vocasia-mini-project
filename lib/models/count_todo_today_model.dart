class CountTodoTodayModel {
  final String msg;
  final int statusCode;
  final Map<String, dynamic> data;

  CountTodoTodayModel({
    required this.msg,
    required this.statusCode,
    required this.data,
  });

  factory CountTodoTodayModel.fromJson(Map<String, dynamic> json) {
    return CountTodoTodayModel(
      msg: json['msg'],
      statusCode: json['status_code'],
      data: json['data'],
    );
  }
}
