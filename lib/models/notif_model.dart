class NotifModel {
  final int id;
  final String title;
  final String description;
  final String finishAt;
  final String createdAt;
  final String updatedAt;

  const NotifModel({
    required this.id,
    required this.title,
    required this.description,
    required this.finishAt,
    required this.createdAt,
    required this.updatedAt,
  });

  factory NotifModel.fromJson(Map<String, dynamic> json) {
    return NotifModel(
      id: int.parse(json['id_todos']),
      title: json['title'],
      description: json['description'],
      finishAt: json['finish_at'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
    );
  }
}
