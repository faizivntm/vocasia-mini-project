class CreateTodoTodayModel {
  final String title;
  final DateTime dateInput;
  CreateTodoTodayModel({
    required this.title,
    required this.dateInput,
  });
}
