class DeleteModels {
  final int status;
  final String msg;

  DeleteModels({required this.status, required this.msg});

  factory DeleteModels.fromJson(Map<String, dynamic> json) {
    return DeleteModels(
      status: json['status'] as int,
      msg: json['msg'] as String,
    );
  }
}
