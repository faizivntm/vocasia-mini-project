class AddTaskModel {
  String? title;
  String? description;
  String? finishAt;

  AddTaskModel({
    required this.title,
    required this.description,
    required this.finishAt,
  });

  factory AddTaskModel.fromJson(Map<String, dynamic> json) {
    return AddTaskModel(
      title: json['title'],
      description: json['description'],
      finishAt: json['finish_at'],
    );
  }
}
