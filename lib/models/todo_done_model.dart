class TodoDoneModel {
  final int id;
  final String title;
  final String description;
  final String finishAt;

  const TodoDoneModel({
    required this.id,
    required this.title,
    required this.description,
    required this.finishAt,
  });

  factory TodoDoneModel.fromJson(Map<String, dynamic> json) {
    return TodoDoneModel(
      id: int.parse(json['id_todos']),
      title: json['title'],
      description: json['description'],
      finishAt: json['finish_at'],
    );
  }
}
