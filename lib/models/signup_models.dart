class SignUpModel {
  String? msg;
  int? statusCode;

  SignUpModel({this.msg, this.statusCode});

  SignUpModel.fromJson(Map<String, dynamic> json) {
    msg = json['msg'];
    statusCode = json['status_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['msg'] = this.msg;
    data['status_code'] = this.statusCode;
    return data;
  }
}
