class TodoOverdueModel {
  final int id;
  final String title;
  final String description;
  final String finishAt;

  const TodoOverdueModel({
    required this.id,
    required this.title,
    required this.description,
    required this.finishAt,
  });

  factory TodoOverdueModel.fromJson(Map<String, dynamic> json) {
    return TodoOverdueModel(
      id: int.parse(json['id_todos']),
      title: json['title'],
      description: json['description'],
      finishAt: json['finish_at'],
    );
  }
}
