import 'package:flutter/material.dart';

import '../../register_screen/views/register_screen.dart';

class RegisterButton extends StatelessWidget {
  const RegisterButton({super.key, required this.onPressed});
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
        "Sign up now.",
        style: TextStyle(
          color: Color.fromRGBO(186, 24, 27, 1),
        ),
      ),
    );
  }
}
