import 'package:flutter/material.dart';

class LoginButton extends StatelessWidget {
  const LoginButton({super.key, required this.onPressed});
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      child: Text("Login",
          style: const TextStyle(color: Colors.white),
          overflow: TextOverflow.ellipsis),
      style: ElevatedButton.styleFrom(
          backgroundColor: Color.fromRGBO(186, 24, 27, 1),
          minimumSize: Size(292, 42),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          )),
    );
  }
}
