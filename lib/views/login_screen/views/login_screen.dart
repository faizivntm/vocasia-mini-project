import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mini_project/providers/login_provider.dart';
import 'package:mini_project/views/login_screen/widget/button_login.dart';
import 'package:mini_project/views/login_screen/widget/text_button_register.dart';
import 'package:mini_project/widgets/popup_widgets/popoup_loading/views/popup_loading.dart';
import 'package:provider/provider.dart';
import '../../../services/shared_services.dart';
import '../../../utils/state/finite_state.dart';
import '../../home_screen/view/home_screen.dart';
import '../../register_screen/views/register_screen.dart';

class LoginScreen extends StatefulWidget {
  static const String routeName = 'login-screen';
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  bool _passwordVisible = true;

  @override
  void initState() {
    final provider = Provider.of<LogInProvider>(context, listen: false);
    provider.addListener(
      () {
        if (provider.myState == MyState.loading) {
          AlertDialog alert = AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            content: Loading(),
          );
          showDialog(context: context, builder: (context) => alert);
          return;
        } else if (provider.myState == MyState.failed) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 5),
              content: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text("Wrong Email and Password!"),
              ),
              backgroundColor: Colors.red,
            ),
          );
          Navigator.pop(context);
        } else if (provider.myState == MyState.loaded) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 5),
              content: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text("Login Successfully!"),
              ),
              backgroundColor: Colors.green,
            ),
          );
          Navigator.pushNamedAndRemoveUntil(
              context, HomeScreen.routeName, (route) => false);
        }
      },
    );
    super.initState();
  }

  final FocusNode _focusNodeEmail = FocusNode();
  final FocusNode _focusNodePass = FocusNode();

  @override
  void dispose() {
    _focusNodeEmail.dispose();
    _focusNodePass.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<LogInProvider>(context, listen: false);
    final size = MediaQuery.of(context).size;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return GestureDetector(
      onTap: () {
        if (_focusNodeEmail.hasFocus) {
          _focusNodeEmail.unfocus();
        }
        if (_focusNodePass.hasFocus) {
          _focusNodePass.unfocus();
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Center(
          child: Container(
            child: Column(
              children: [
                Spacer(),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Text(
                    "Login",
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 24),
                  ),
                ),
                SizedBox(
                  height: size.height * 0.040,
                ),
                Container(
                  height: size.height * 0.065,
                  width: size.width * 0.815,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Colors.blueGrey,
                      )),
                  padding: EdgeInsets.only(left: 20, top: 1),
                  child: TextFormField(
                    focusNode: _focusNodeEmail,
                    controller: _emailController,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      hintText: 'input email',
                      labelStyle: TextStyle(
                        color: Colors.blueGrey,
                      ),
                    ),
                    onChanged: (value) {},
                  ),
                ),
                SizedBox(
                  height: size.height * 0.024,
                ),
                Container(
                  height: size.height * 0.065,
                  width: size.width * 0.815,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      border: Border.all(
                        color: Colors.blueGrey,
                      )),
                  padding: EdgeInsets.only(left: 20, top: 1, right: 7),
                  child: TextFormField(
                    focusNode: _focusNodePass,
                    keyboardType: TextInputType.text,
                    controller: _passwordController,
                    obscureText: _passwordVisible,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Enter your password',
                      suffixIcon: IconButton(
                        icon: Icon(
                          _passwordVisible
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          color: Colors.black,
                        ),
                        onPressed: () {
                          setState(() {
                            _passwordVisible = !_passwordVisible;
                          });
                        },
                      ),
                    ),
                  ),
                ),
                Container(
                  height: size.height * 0.055,
                  width: size.width * 0.815,
                  alignment: Alignment.centerRight,
                  child: TextButton(
                      onPressed: () => Navigator.pushReplacementNamed(
                          context, RegisterScreen.routeName),
                      child: Text(
                        "Forgot password?",
                        style: TextStyle(
                          color: Color.fromRGBO(186, 24, 27, 1),
                        ),
                      )),
                ),
                SizedBox(
                  height: 36,
                ),
                Container(
                    alignment: Alignment.center,
                    child: Container(
                      height: size.height * 0.055,
                      width: size.width * 0.815,
                      child: LoginButton(
                        onPressed: (() async {
                          final prf = SharedService();
                          await provider.signIn(
                            email: _emailController.text,
                            password: _passwordController.text,
                          );
                        }),
                      ),
                    )),
                Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Don't have an account?",
                        style:
                            const TextStyle(color: Colors.black, fontSize: 15),
                        overflow: TextOverflow.ellipsis),
                    RegisterButton(onPressed: ((() {
                      Navigator.pushReplacementNamed(
                          context, RegisterScreen.routeName);
                    })))
                  ],
                )),
                Spacer()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
