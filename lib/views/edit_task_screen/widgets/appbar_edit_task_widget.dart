import 'package:flutter/material.dart';
import 'package:mini_project/utils/constant.dart';

PreferredSizeWidget appBarEditTaskWidget(BuildContext context) {
  final size = MediaQuery.of(context).size;
  return PreferredSize(
    preferredSize: Size.fromHeight(size.height * 0.120),
    child: AppBar(
      toolbarHeight: size.height * 0.1200,
      leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: Image.asset(
            'assets/icons/icon_arrow_left.png',
            height: size.height * 0.035,
          )),
      elevation: 0,
      backgroundColor: Colors.white,
      centerTitle: true,
      title: Text(
        'Edit Task',
        style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w600,
            fontSize: Constant.fontTitle),
      ),
    ),
  );
}
