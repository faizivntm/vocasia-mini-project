import 'package:date_picker_timeline/date_picker_timeline.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mini_project/models/add_task_model.dart';
import 'package:mini_project/providers/add_task_provider.dart';
import 'package:mini_project/services/services.dart';
import 'package:mini_project/views/add_task_screen/widgets/appbar_add_task_widget.dart';
import 'package:mini_project/views/add_task_screen/widgets/button_notification_widget.dart';
import 'package:mini_project/views/add_task_screen/widgets/button_repeat_widget%20.dart';
import 'package:mini_project/views/add_task_screen/widgets/button_tag_widget.dart';
import 'package:mini_project/views/add_task_screen/widgets/button_task_time_widget.dart';
import 'package:provider/provider.dart';

import '../../../models/create_todo_today_model.dart';
import '../../../utils/constant.dart';
import '../../../utils/state/finite_state.dart';
import '../../home_screen/view/home_screen.dart';

class AddTaskScreen extends StatefulWidget {
  static const String routeName = 'add-task-screen';
  const AddTaskScreen({Key? key}) : super(key: key);

  @override
  State<AddTaskScreen> createState() => _AddTaskScreenState();
}

class _AddTaskScreenState extends State<AddTaskScreen> {
  final formKey = GlobalKey<FormState>();
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  DatePickerController _controller = DatePickerController();
  DateTime _selectedDayValue = DateTime.now();
  DateFormat dateFormatter = DateFormat('yyyy-MM-dd');

  // Function TimePicker
  TimeOfDay _timeOfDay = TimeOfDay.now();
  void _showTimePicker() {
    showTimePicker(
      context: context,
      initialTime: TimeOfDay.now(),
    ).then((value) {
      setState(() {
        _timeOfDay = value!;
      });
    });
  }

  @override
  void initState() {
    _timeOfDay;
    _selectedDayValue;

    final provider = Provider.of<AddTaskProvider>(context, listen: false);
    provider.addListener(
      () {
        if (provider.myState == MyState.failed) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 5),
              content: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text("Failed!"),
              ),
              backgroundColor: Colors.red,
            ),
          );
        } else if (provider.myState == MyState.loaded) {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              duration: const Duration(seconds: 5),
              content: Container(
                height: 30,
                alignment: Alignment.center,
                child: Text("Successfully!"),
              ),
              backgroundColor: Colors.green,
            ),
          );
          Navigator.pushNamedAndRemoveUntil(
              context, HomeScreen.routeName, (route) => false);
        }
      },
    );
    super.initState();
  }

  final FocusNode _focusNodeTitle = FocusNode();
  final FocusNode _focusNodeDescription = FocusNode();

  @override
  void dispose() {
    _focusNodeTitle.dispose();
    _focusNodeDescription.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AddTaskProvider>(context, listen: false);
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        if (_focusNodeTitle.hasFocus) {
          _focusNodeTitle.unfocus();
        }
        if (_focusNodeDescription.hasFocus) {
          _focusNodeDescription.unfocus();
        }
      },
      child: Scaffold(
        appBar: appBarAddTaskWidget(context),
        body: FutureBuilder(builder: (context, snapshot) {
          String date = DateFormat('yyyy-MM-dd').format(_selectedDayValue);
          final clock = DateFormat('HH:mm:ss').format(DateTime(
            DateTime.now().year,
            DateTime.now().month,
            DateTime.now().day,
            _timeOfDay.hour,
            _timeOfDay.minute,
            0,
          ));
          final finishAt = '${date} ${clock}';
          return ListView(padding: EdgeInsets.all(24.0), children: [
            Column(children: [
              //Space
              SizedBox(
                height: size.height * 0.030,
                width: size.width * 0.030,
              ),
              // DateTime
              Container(
                child: DatePicker(
                  height: size.height * 0.120,
                  width: size.width * 0.180,
                  DateTime.now(),
                  initialSelectedDate: DateTime.now(),
                  selectionColor: Color(Constant.colorRedBA181B),
                  controller: _controller,
                  onDateChange: (selectedDate) {
                    setState(() {
                      _selectedDayValue = selectedDate;
                    });
                  },
                ),
              ),
              //Space
              SizedBox(
                height: size.height * 0.030,
                width: size.width * 0.030,
              ),
              //TextFormField
              Form(
                key: formKey,
                child: Column(
                  children: [
                    TextFormField(
                      focusNode: _focusNodeTitle,
                      controller: titleController,
                      maxLines: 1,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(
                          top: 25.0,
                          right: 30.0,
                          left: 30.0,
                          bottom: 25.0,
                        ),
                        hintText: 'Title',
                        filled: true,
                        fillColor: Color(Constant.colorWhiteFBFBFB),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(Constant.colorWhiteFBFBFB)),
                            borderRadius: BorderRadius.circular(30.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(Constant.colorWhiteFBFBFB)),
                            borderRadius: BorderRadius.circular(30.0)),
                      ),
                    ),

                    //Space
                    SizedBox(
                      height: size.height * 0.010,
                      width: size.width * 0.030,
                    ),

                    TextFormField(
                      focusNode: _focusNodeDescription,
                      controller: descriptionController,
                      maxLines: 6,
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.only(
                          top: 45.0,
                          right: 30.0,
                          left: 30.0,
                          bottom: 45.0,
                        ),
                        hintText: 'Mini Project Vocasia',
                        filled: true,
                        fillColor: Color(Constant.colorWhiteFBFBFB),
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(Constant.colorWhiteFBFBFB)),
                            borderRadius: BorderRadius.circular(30.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                                color: Color(Constant.colorWhiteFBFBFB)),
                            borderRadius: BorderRadius.circular(30.0)),
                      ),
                    ),
                  ],
                ),
              ),

              //Space
              SizedBox(
                height: size.height * 0.030,
                width: size.width * 0.030,
              ),

              // Task Time
              ButtonTaskTimeWidget(
                onTap: _showTimePicker,
                textTime: '${DateFormat('HH:mm').format(DateTime(
                  DateTime.now().year,
                  DateTime.now().month,
                  DateTime.now().day,
                  _timeOfDay.hour,
                  _timeOfDay.minute,
                ))}',
              ),
              //Space
              SizedBox(
                height: size.height * 0.030,
                width: size.width * 0.030,
              ),
              // Notifaction
              ButtonNotificationWidget(),
              //Space
              SizedBox(
                height: size.height * 0.030,
                width: size.width * 0.030,
              ),
              // Repeat
              ButtonRepeatWidget(),
              //Space
              SizedBox(
                height: size.height * 0.030,
                width: size.width * 0.030,
              ),
              // Tag
              ButtonTagWidget(),
              //Space
              SizedBox(
                height: size.height * 0.050,
                width: size.width * 0.050,
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    // Button Cancel
                    Center(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.only(
                              top: 16.0, bottom: 16.0, right: 40.0, left: 40.0),
                          backgroundColor: Color(Constant.colorGreyCCCCCC),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0)),
                          elevation: 0,
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text(
                          'Cancel',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: Constant.fontSemiRegular),
                        ),
                      ),
                    ),
                    // Button Add Task
                    Center(
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          padding: EdgeInsets.only(
                              top: 16.0, bottom: 16.0, right: 40.0, left: 40.0),
                          backgroundColor: Color(Constant.colorRedBA181B),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0)),
                          elevation: 0,
                        ),
                        onPressed: () async {
                          await provider.addTask(
                            title: titleController.text,
                            description: descriptionController.text,
                            finsihAt: finishAt,
                          );
                        },
                        child: Text(
                          'Save Task',
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: Constant.fontSemiRegular),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ]),
          ]);
        }),
      ),
    );
  }
}
