import 'package:flutter/material.dart';

import '../../../utils/constant.dart';

class ButtonRepeatWidget extends StatelessWidget {
  const ButtonRepeatWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {},
      child: Container(
        height: size.height * 0.100,
        width: size.width * 0.900,
        decoration: BoxDecoration(
          color: Color(Constant.colorWhiteFBFBFB),
          borderRadius: BorderRadius.circular(40.0),
        ),
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  children: [
                    Image.asset('assets/icons/icon_repeat.png',
                        height: size.height * 0.040),
                    //Space
                    SizedBox(
                      width: size.width * 0.030,
                    ),
                    Text(
                      'Repeat',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: Constant.fontSemiBig),
                    ),
                  ],
                ),
              ),
              Expanded(
                  child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'No',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: Constant.fontSemiRegular),
                  ),
                  //Space
                  SizedBox(
                    width: size.width * 0.010,
                  ),
                  Image.asset(
                    'assets/icons/icon_arrow_chevron_right.png',
                    height: size.height * 0.035,
                    fit: BoxFit.cover,
                  ),
                ],
              )),
            ],
          ),
        ),
      ),
    );
  }
}
