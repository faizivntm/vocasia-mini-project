import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mini_project/providers/add_todo_done_provider.dart';
import 'package:mini_project/views/add_task_screen/views/add_task_screen.dart';
import 'package:mini_project/views/home_screen/widget/profile_home_widgets.dart';
import 'package:mini_project/views/home_screen/widget/tab_bar_widgets/tab_bar_controller_widgets/tab_bar_controller_todo_overdue_widget.dart';
import 'package:mini_project/views/home_screen/widget/tab_bar_widgets/tab_bar_controller_widgets/tab_bar_controller_todo_today_widget.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../../providers/user_provider.dart';
import '../../../utils/constant.dart';
import '../widget/tab_bar_widgets/tab_bar_controller_widgets/tab_bar_controller_todo_done_widget.dart';
import '../widget/tab_bar_widgets/tab_bar_view_widgets/tab_bar_view_done_widget.dart';
import '../widget/tab_bar_widgets/tab_bar_view_widgets/tab_bar_view_overdue_widget.dart';
import '../widget/tab_bar_widgets/tab_bar_view_widgets/tab_bar_view_to_do_widget.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = 'home-screen';
  const HomeScreen({
    Key? key,
    this.selectedIndex = 0,
  }) : super(key: key);
  final int selectedIndex;
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late SharedPreferences loginData;
  String username = '';

  void initial() async {
    loginData = await SharedPreferences.getInstance();
  }

  late TabController _tabController;
  @override
  void initState() {
    _tabController = TabController(
        length: 3, vsync: this, initialIndex: widget.selectedIndex);
    final profileProvider = Provider.of<UserProvider>(context, listen: false);
    profileProvider.fetchProfile();
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('EEEE, d MMM yyyy').format(now);
    final size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              // Widget Profile Home
              ProfileHomeWidgets(),
              SizedBox(
                height: size.height * 0.040,
                width: size.width * 0.040,
              ),
              // Tanggal
              Text(
                '${formattedDate}',
                style: TextStyle(
                    color: Color(Constant.colorTextDateGrey),
                    fontSize: Constant.fontSemiRegular),
              ),
              // Space
              SizedBox(
                height: size.height * 0.010,
                width: size.width * 0.020,
              ),
              Container(
                decoration:
                    BoxDecoration(borderRadius: BorderRadius.circular(20)),
                child: Container(
                  padding: EdgeInsets.all(15),
                  height: size.height * 0.070,
                  width: size.width * 0.850,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      borderRadius: BorderRadius.circular(20)),
                  child: Row(
                    children: [
                      Container(
                        alignment: Alignment.centerLeft,
                        child: const Icon(
                          Icons.search,
                          size: 24.0,
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(
                        width: size.width * 0.025,
                      ),
                      Container(
                        child: Text(
                          "Search Task",
                          style: TextStyle(
                              fontSize: 18,
                              color: Colors.grey,
                              fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              // Space
              SizedBox(
                height: size.height * 0.025,
                width: size.width * 0.040,
              ),
              // Report
              Text(
                'Report',
                style: TextStyle(
                    fontSize: Constant.fontSemiBig,
                    fontWeight: FontWeight.w600),
              ),
              // Space
              SizedBox(
                height: size.height * 0.010,
                width: size.width * 0.030,
              ),
              // TabBar
              SizedBox(
                height: size.height * 0.120,
                child: TabBar(
                    controller: _tabController,
                    labelPadding:
                        EdgeInsets.symmetric(vertical: 2.0, horizontal: 2.0),
                    tabs: [
                      // Tab Task To Do
                      TabBarControllerTodoTodayWidget(),
                      // Tab Task Overdue
                      TabBarControllerTodoOverdueWidget(),
                      // Tab Task Done
                      TabBarControllerTodoDoneWidget()
                    ]),
              ),
              // Space
              SizedBox(
                height: size.height * 0.015,
                width: size.width * 0.040,
              ),
              // TabBarView
              SizedBox(
                height: size.height * 0.370,
                width: size.width * 0.500,
                child: TabBarView(controller: _tabController, children: [
                  TabBarViewToDoWidget(),
                  TabBarViewOverdueWidget(),
                  TabBarViewDoneWidget(),
                ]),
              ),
              // Space
              SizedBox(
                height: size.height * 0.040,
                width: size.width * 0.040,
              ),
              // Button Add Task
              Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    padding: EdgeInsets.only(
                        top: 14.0, bottom: 14.0, right: 40.0, left: 40.0),
                    backgroundColor: Color(Constant.colorRedBA181B),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(12.0)),
                    elevation: 0,
                  ),
                  onPressed: () {
                    Navigator.pushNamed(context, AddTaskScreen.routeName);
                  },
                  child: Text(
                    'Add Task',
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: Constant.fontSemiRegular),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
