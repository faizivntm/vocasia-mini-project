import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:mini_project/models/user_model.dart';
import 'package:mini_project/providers/notif_provider.dart';
import 'package:mini_project/views/notification_screen/views/notification_screen.dart';
import 'package:mini_project/widgets/popup_widgets/popup_logout/views/popup_logout.dart';
import 'package:provider/provider.dart';
import '../../../services/services.dart';
import '../../../utils/constant.dart';
import '../../../providers/user_provider.dart';

class ProfileHomeWidgets extends StatefulWidget {
  const ProfileHomeWidgets({super.key});

  @override
  State<ProfileHomeWidgets> createState() => _ProfileHomeWidgetsState();
}

class _ProfileHomeWidgetsState extends State<ProfileHomeWidgets> {
  ApiServices services = ApiServices();

  Future<UserModel> fetchUsers() async {
    return await services.fetchUsers();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return FutureBuilder<UserModel>(
      future: fetchUsers(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final data = snapshot.data!;

          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              PopupLogout(),
              SizedBox(
                width: size.width * 0.030,
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Welcome.',
                        style: TextStyle(fontSize: Constant.fontRegular)),
                    SizedBox(
                      height: size.height * 0.010,
                      width: size.width * 0.010,
                    ),
                    Row(
                      children: [
                        Flexible(
                            child: Text(data.username,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                    fontSize: Constant.fontBig,
                                    fontWeight: FontWeight.bold))),
                        Container(
                          child: Text(
                            ' 👋🏻 ',
                            style: TextStyle(fontSize: Constant.fontBig),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 10),
                child: IconButton(
                  onPressed: () {
                    Navigator.pushNamed(context, NotifScreen.routeName);
                  },
                  icon: const Icon(
                    Icons.notifications_outlined,
                    size: 30.0,
                  ),
                ),
              ),
            ],
          );
        } else if (snapshot.hasError) {
          return Text('Error fetching data');
        } else {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              PopupLogout(),
              SizedBox(
                width: size.width * 0.030,
              ),
              Flexible(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Welcome.',
                        style: TextStyle(fontSize: Constant.fontRegular)),
                    SizedBox(
                      height: size.height * 0.010,
                      width: size.width * 0.010,
                    ),
                    Row(
                      children: [
                        Flexible(
                          child: SizedBox(
                            child: LoadingAnimationWidget.staggeredDotsWave(
                                color: Color.fromRGBO(186, 24, 27, 1),
                                size: 20),
                          ),
                        ),
                        Container(
                          child: Text(
                            ' 👋🏻 ',
                            style: TextStyle(fontSize: Constant.fontBig),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(right: 10),
                child: IconButton(
                  onPressed: () {
                    Navigator.pushNamed(context, NotifScreen.routeName);
                  },
                  icon: const Icon(
                    Icons.notifications_outlined,
                    size: 30.0,
                  ),
                ),
              ),
            ],
          );
        }
      },
    );
    // return Consumer<UserProvider>(builder: (context, provider, snapshot) {
    //   return Row(
    //     mainAxisAlignment: MainAxisAlignment.spaceAround,
    //     children: [
    //       PopupLogout(),
    //       SizedBox(
    //         width: size.width * 0.030,
    //       ),
    //       Flexible(
    //         child: Column(
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: [
    //             Text('Welcome.',
    //                 style: TextStyle(fontSize: Constant.fontRegular)),
    //             SizedBox(
    //               height: size.height * 0.010,
    //               width: size.width * 0.010,
    //             ),
    //             Row(
    //               children: [
    //                 Flexible(
    //                     child: Text('${provider.profileModel?.username ?? " "}',
    //                         overflow: TextOverflow.ellipsis,
    //                         style: TextStyle(
    //                             fontSize: Constant.fontBig,
    //                             fontWeight: FontWeight.bold))),
    //                 Container(
    //                   child: Text(
    //                     ' 👋🏻 ',
    //                     style: TextStyle(fontSize: Constant.fontBig),
    //                   ),
    //                 ),
    //               ],
    //             ),
    //           ],
    //         ),
    //       ),
    //       Container(
    //         padding: EdgeInsets.only(right: 10),
    //         child: IconButton(
    //           onPressed: () {
    //             Navigator.pushNamed(context, NotifScreen.routeName);
    //           },
    //           icon: const Icon(
    //             Icons.notifications_outlined,
    //             size: 30.0,
    //           ),
    //         ),
    //       ),
    //     ],
    //   );
    // });
  }
}
