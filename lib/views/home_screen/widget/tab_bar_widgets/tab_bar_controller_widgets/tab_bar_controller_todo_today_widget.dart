import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import '../../../../../models/count_todo_today_model.dart';
import '../../../../../models/todo_overdue_model.dart';
import '../../../../../models/todo_today_model.dart';
import '../../../../../services/services.dart';
import '../../../../../utils/constant.dart';

class TabBarControllerTodoTodayWidget extends StatefulWidget {
  const TabBarControllerTodoTodayWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<TabBarControllerTodoTodayWidget> createState() =>
      _TabBarControllerTodoTodayWidgetState();
}

class _TabBarControllerTodoTodayWidgetState
    extends State<TabBarControllerTodoTodayWidget> {
  ApiServices services = ApiServices();

  Future<List<TodoTodayModel>> _fetchTodoToday() async {
    return await services.fetchTodoToday();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/task_to_do_shape.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: FutureBuilder<List<TodoTodayModel>>(
          future: _fetchTodoToday(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final data = snapshot.data!;

              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        data.length.toString(),
                        style: TextStyle(
                          fontSize: Constant.fontBig,
                          color: Colors.white,
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Task',
                            style: TextStyle(
                              fontSize: Constant.fontRegular,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            'Today',
                            style: TextStyle(
                              fontSize: Constant.fontRegular,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('Error fetching data');
            } else {
              return Center(
                child: LoadingAnimationWidget.staggeredDotsWave(
                    color: Color.fromRGBO(186, 24, 27, 1), size: 20),
              );
            }
          },
        ),
      ),
    );
  }
}
