// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';

import '../../../../../models/todo_overdue_model.dart';
import '../../../../../services/services.dart';
import '../../../../../utils/constant.dart';

class TabBarControllerTodoOverdueWidget extends StatefulWidget {
  const TabBarControllerTodoOverdueWidget({Key? key}) : super(key: key);

  @override
  State<TabBarControllerTodoOverdueWidget> createState() =>
      _TabBarControllerTodoOverdueWidgetState();
}

class _TabBarControllerTodoOverdueWidgetState
    extends State<TabBarControllerTodoOverdueWidget> {
  ApiServices services = ApiServices();

  Future<List<TodoOverdueModel>> _fetchTodoOverdue() async {
    return await services.fetchTodoOverdue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/task_overdue_shape.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: FutureBuilder<List<TodoOverdueModel>>(
          future: _fetchTodoOverdue(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final data = snapshot.data!;

              return Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        data.length.toString(),
                        style: TextStyle(
                          fontSize: Constant.fontBig,
                          color: Colors.white,
                        ),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Task',
                            style: TextStyle(
                              fontSize: Constant.fontRegular,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            'Overdue',
                            style: TextStyle(
                              fontSize: Constant.fontRegular,
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text('Error fetching data');
            } else {
              return Center(
                  child: Container(
                child: LoadingAnimationWidget.staggeredDotsWave(
                    color: Color.fromRGBO(186, 24, 27, 1), size: 20),
              ));
            }
          },
        ),
      ),
    );
  }
}
