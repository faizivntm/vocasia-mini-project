import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mini_project/widgets/popup_widgets/popup_delete_tatsk/views/popup_delete_task.dart';
import 'package:provider/provider.dart';

import '../../../../../models/todo_overdue_model.dart';
import '../../../../../providers/add_todo_done_provider.dart';
import '../../../../../services/services.dart';
import '../../../../../utils/constant.dart';
import '../../../../../widgets/popup_widgets/popup_detail_task/views/popup_detail_task.dart';

class TabBarViewOverdueWidget extends StatefulWidget {
  const TabBarViewOverdueWidget({Key? key}) : super(key: key);

  @override
  State<TabBarViewOverdueWidget> createState() =>
      _TabBarViewOverdueWidgetState();
}

class _TabBarViewOverdueWidgetState extends State<TabBarViewOverdueWidget> {
  List<TodoOverdueModel> data = [];
  ApiServices services = ApiServices();

  fetchTodoOverdue() async {
    List<TodoOverdueModel> dataTodoOverdue = await services.fetchTodoOverdue();
    setState(() {
      data = dataTodoOverdue;
    });
  }

  @override
  void initState() {
    fetchTodoOverdue();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<AddTodoDoneProvider>(context, listen: false);
    int todoOverdueLength = data.length;
    final size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Overdue',
              style: TextStyle(
                  fontSize: Constant.fontSemiBig, fontWeight: FontWeight.bold),
            ),
            TextButton(
              onPressed: () {},
              child: Text(
                'See All',
                style: TextStyle(
                    fontSize: Constant.fontRegular,
                    color: Color(Constant.colorTextDateGrey)),
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                String dateApi = data[index].finishAt;
                DateTime dateTime = DateTime.parse(dateApi);
                DateFormat dateFormatter = DateFormat('EEE, dd MMM yyyy');
                DateFormat timeFormatter = DateFormat('HH:mm');
                String date = dateFormatter.format(dateTime);
                String clock = timeFormatter.format(dateTime);
                String title = data[index].title;
                String todoOverdue = "todoOverdue";
                return Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: GestureDetector(
                      onTap: () {
                        showModalBottomSheet(
                          context: context,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25),
                          ),
                          builder: (_) => DetailTask(
                            date: date,
                            clock: clock,
                            title: title,
                            id: data[index].id,
                            todo: "",
                            todoDone: "",
                            todoOverdue: todoOverdue,
                          ),
                        );
                      },
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            color: Color(Constant.colorWhiteFBFBFB),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Row(children: [
                              Container(
                                height: size.height * 0.060,
                                width: size.width * 0.010,
                                decoration: BoxDecoration(
                                    color: Color(Constant.colorGreen1BA345),
                                    borderRadius: BorderRadius.circular(15.0)),
                              ),
                              SizedBox(
                                width: size.width * 0.035,
                              ),
                              Flexible(
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Flexible(
                                              child: Text(
                                                  title = data[index].title,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontSize:
                                                          Constant.fontSemiBig,
                                                      fontWeight:
                                                          FontWeight.bold))),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 12.0, bottom: 3.0),
                                            child: Container(
                                              height: size.height * 0.022,
                                              width: size.width * 0.100,
                                              decoration: BoxDecoration(
                                                color: Color(
                                                    Constant.colorGreen1BA345),
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                              ),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(2.0),
                                                child: Text(
                                                  'Daily',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          Constant.fontSmall),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: size.height * 0.010,
                                        width: size.width * 0.010,
                                      ),
                                      Text('${date} | ${clock}',
                                          style: TextStyle(
                                              fontSize: Constant.fontSemiSmall,
                                              color: Color(
                                                  Constant.colorTextDateGrey))),
                                    ]),
                              ),
                              Transform.scale(
                                  scale: 1.2,
                                  child: IconButton(
                                      onPressed: () async {
                                        await provider
                                            .addTodoDone(data[index].id);
                                      },
                                      icon: Image.asset(
                                          'assets/icons/icon_checklist_grey.png'))),
                            ]),
                          )),
                    ));
              }),
        ),
      ],
    );
  }
}
