import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../../../../../models/todo_done_model.dart';
import '../../../../../services/services.dart';
import '../../../../../utils/constant.dart';
import '../../../../../widgets/popup_widgets/popup_delete_tatsk/views/popup_delete_task.dart';
import '../../../../../widgets/popup_widgets/popup_detail_task/views/popup_detail_task.dart';

class TabBarViewDoneWidget extends StatefulWidget {
  const TabBarViewDoneWidget({Key? key}) : super(key: key);

  @override
  State<TabBarViewDoneWidget> createState() => _TabBarViewDoneWidgetState();
}

class _TabBarViewDoneWidgetState extends State<TabBarViewDoneWidget> {
  List<TodoDoneModel> data = [];
  ApiServices services = ApiServices();

  fetchTodoDone() async {
    List<TodoDoneModel> dataTodoDone = await services.fetchTodoDone();
    setState(() {
      data = dataTodoDone;
    });
  }

  @override
  void initState() {
    fetchTodoDone();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    int todoDoneLength = data.length;

    final size = MediaQuery.of(context).size;
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              'Done',
              style: TextStyle(
                  fontSize: Constant.fontSemiBig, fontWeight: FontWeight.bold),
            ),
            TextButton(
              onPressed: () {},
              child: Text(
                'See All',
                style: TextStyle(
                    fontSize: Constant.fontRegular,
                    color: Color(Constant.colorTextDateGrey)),
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                String dateApi = data[index].finishAt;
                DateTime dateTime = DateTime.parse(dateApi);
                DateFormat dateFormatter = DateFormat('EEE, dd MMM yyyy');
                DateFormat timeFormatter = DateFormat('HH:mm');
                String date = dateFormatter.format(dateTime);
                String clock = timeFormatter.format(dateTime);
                String title = data[index].title;
                String todoDone = "todoDone";
                return Padding(
                    padding: const EdgeInsets.only(bottom: 5.0),
                    child: GestureDetector(
                      onTap: () {
                        showModalBottomSheet(
                          context: context,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25),
                          ),
                          builder: (_) => DetailTask(
                            date: date,
                            clock: clock,
                            title: title,
                            id: data[index].id,
                            todo: "",
                            todoOverdue: "",
                            todoDone: todoDone,
                          ),
                        );
                      },
                      child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            color: Color(Constant.colorWhiteFBFBFB),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Row(children: [
                              Container(
                                height: size.height * 0.060,
                                width: size.width * 0.010,
                                decoration: BoxDecoration(
                                    color: Color(Constant.colorRedBA181B),
                                    borderRadius: BorderRadius.circular(15.0)),
                              ),
                              SizedBox(
                                width: size.width * 0.035,
                              ),
                              Flexible(
                                flex: 2,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        children: [
                                          Flexible(
                                              child: Text(
                                                  title = data[index].title,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      decoration: TextDecoration
                                                          .lineThrough,
                                                      fontSize:
                                                          Constant.fontSemiBig,
                                                      fontWeight:
                                                          FontWeight.bold))),
                                          Padding(
                                            padding: const EdgeInsets.only(
                                                left: 12.0, bottom: 3.0),
                                            child: Container(
                                              height: size.height * 0.022,
                                              width: size.width * 0.100,
                                              decoration: BoxDecoration(
                                                color: Color(
                                                    Constant.colorRedBA181B),
                                                borderRadius:
                                                    BorderRadius.circular(8.0),
                                              ),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(2.0),
                                                child: Text(
                                                  'lmp',
                                                  textAlign: TextAlign.center,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          Constant.fontSmall),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: size.height * 0.010,
                                        width: size.width * 0.010,
                                      ),
                                      Text("${date} | ${clock}",
                                          style: TextStyle(
                                              fontSize: Constant.fontSemiSmall,
                                              color: Color(
                                                  Constant.colorTextDateGrey))),
                                    ]),
                              ),
                              Transform.scale(
                                  scale: 0.9,
                                  child: IconButton(
                                      onPressed: () {},
                                      icon: Image.asset(
                                          'assets/icons/icon_ceklis.png'))),
                            ]),
                          )),
                    ));
              }),
        ),
      ],
    );
  }
}
