import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:lottie/lottie.dart';

class EmptyWidget extends StatelessWidget {
  static const routeName = 'empty-widget';
  const EmptyWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Lottie.asset('assets/animate/empty_page.json',
            height: size.height * 0.250)
      ]),
    );
  }
}
