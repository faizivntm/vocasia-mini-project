import 'package:flutter/material.dart';
import '../../../models/notif_model.dart';
import '../../../services/services.dart';
import '../../home_screen/view/home_screen.dart';

class NotifScreen extends StatefulWidget {
  static const String routeName = 'notif-screen';
  const NotifScreen({Key? key}) : super(key: key);

  @override
  State<NotifScreen> createState() => _NotifScreenState();
}

class _NotifScreenState extends State<NotifScreen> {
  List<NotifModel> data = [];
  ApiServices services = ApiServices();

  getNotifData() async {
    List<NotifModel> _data = await services.getNotifData();
    setState(() {
      data = _data;
    });
  }

  @override
  void initState() {
    getNotifData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white, //No more green
        elevation: 0.0,

        centerTitle: true,
        title: const Text(
          "Notification",
          style: const TextStyle(
            color: Colors.black,
            fontSize: 25,
          ),
        ),
        leading: IconButton(
            icon: Icon(Icons.arrow_back_ios_new_rounded, color: Colors.black),
            onPressed: ((() {
              Navigator.of(context).pop();
            }))),
      ),
      body: Center(
        child: FutureBuilder(
          builder: (context, snapshot) {
            if (data.isEmpty) {
              return Container(
                child: Image.asset("assets/images/vocasia3.png"),
              );
            } else {
              return Scaffold(
                  body: Container(
                child: ListView.separated(
                  padding: EdgeInsets.only(top: 20, right: 30, left: 30),
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          color: Color.fromRGBO(186, 24, 27, 1),
                          borderRadius: BorderRadius.circular(25)),
                      child: Column(
                        children: [
                          Container(
                            height: size.height * 0.055,
                            width: size.width * 0.80,
                            child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Container(
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Icon(
                                        Icons.notification_important,
                                        color: Colors.white,
                                        size: 40,
                                      ),
                                      SizedBox(
                                        width: size.width * 0.050,
                                      ),
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              data[index].title,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 20),
                                            ),
                                            SizedBox(
                                              height: size.height * 0.010,
                                            ),
                                            Text(
                                              data[index].finishAt,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 10),
                                              textAlign: TextAlign.left,
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )),
                          )
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                  itemCount: data.length,
                ),
              ));
            }
          },
        ),
      ),
    );
  }
}
