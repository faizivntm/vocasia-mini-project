import 'dart:async';

import 'package:flutter/material.dart';

import '../login_screen/views/login_screen.dart';


class SplashScreen extends StatefulWidget {
  static const String routeName = 'splash-screen';
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), (() {
      Navigator.pushReplacementNamed(context, LoginScreen.routeName);
    }));
  }

  Widget build(BuildContext context) {
    return Material(
      child: Container(
        decoration: const BoxDecoration(color: Colors.white),
        child: Center(
          child: Container(
            child: Wrap(
              children: [
                Column(
                  children: [
                    Image.asset(
                      "assets/images/vocasia.png",
                      height: 59,
                      width: 147,
                      fit: BoxFit.fill,
                    ),
                    SizedBox(
                      height: 44,
                    ),
                    Image.asset(
                      "assets/images/vocasia2.png",
                      height: 218,
                      width: 283,
                      fit: BoxFit.fill,
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
