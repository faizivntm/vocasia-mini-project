import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({super.key, required this.onPressed});
  final VoidCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onPressed,
      child: Text(
        "Login here.",
        style: TextStyle(
          color: Color.fromRGBO(186, 24, 27, 1),
        ),
      ),
    );
  }
}
