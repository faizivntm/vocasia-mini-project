import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mini_project/views/register_screen/widget/button_sign_up.dart';
import 'package:mini_project/views/register_screen/widget/text_button_login.dart';
import 'package:mini_project/widgets/popup_widgets/popup_failed_register/views/popup_failed_register.dart';
import 'package:mini_project/widgets/popup_widgets/popup_success_register/views/popup_success_register.dart';
import 'package:mini_project/providers/signup_provider.dart';
import 'package:provider/provider.dart';
import '../../../utils/state/finite_state.dart';
import '../../login_screen/views/login_screen.dart';

class RegisterScreen extends StatefulWidget {
  static const String routeName = 'register-screen';
  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmController = TextEditingController();
  bool _passwordVisible = true;
  bool _confirmVisible = true;

  @override
  void initState() {
    final provider = Provider.of<SignUpProvider>(context, listen: false);
    provider.addListener(
      () {
        if (provider.myState == MyState.failed) {
          AlertDialog alert = AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            content: FailedRegister(),
          );
          showDialog(context: context, builder: (context) => alert);
          return;
        } else if (provider.myState == MyState.loaded) {
          AlertDialog alert = AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(25.0))),
            content: SuccessRegister(),
          );

          showDialog(context: context, builder: (context) => alert);
          return;
        }
      },
    );
    super.initState();
  }

  final FocusNode _focusNodeEmail = FocusNode();
  final FocusNode _focusNodePass = FocusNode();
  final FocusNode _focusNodeUsername = FocusNode();
  final FocusNode _focusNodeConfirm = FocusNode();

  @override
  void dispose() {
    _focusNodeEmail.dispose();
    _focusNodePass.dispose();
    _focusNodeUsername.dispose();
    _focusNodeConfirm.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<SignUpProvider>(context, listen: false);
    final size = MediaQuery.of(context).size;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return GestureDetector(
      onTap: () {
        if (_focusNodeEmail.hasFocus) {
          _focusNodeEmail.unfocus();
        }
        if (_focusNodePass.hasFocus) {
          _focusNodePass.unfocus();
        }
        if (_focusNodeConfirm.hasFocus) {
          _focusNodeConfirm.unfocus();
        }
        if (_focusNodeUsername.hasFocus) {
          _focusNodeUsername.unfocus();
        }
      },
      child: Scaffold(
          body: Column(children: [
        Spacer(),
        Container(
          child: Text(
            "Register",
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
        ),
        SizedBox(
          height: size.height * 0.040,
        ),
        Container(
          height: size.height * 0.065,
          width: size.width * 0.815,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: Colors.blueGrey,
              )),
          padding: EdgeInsets.only(left: 20, top: 1),
          child: TextFormField(
            focusNode: _focusNodeUsername,
            controller: _nameController,
            decoration: const InputDecoration(
              border: InputBorder.none,
              hintText: 'Input Name',
              labelStyle: TextStyle(
                color: Colors.blueGrey,
              ),
            ),
            onChanged: (value) {},
          ),
        ),
        SizedBox(
          height: size.height * 0.020,
        ),
        Container(
          height: size.height * 0.065,
          width: size.width * 0.815,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: Colors.blueGrey,
              )),
          padding: EdgeInsets.only(left: 20, top: 1),
          child: TextFormField(
            focusNode: _focusNodeEmail,
            controller: _emailController,
            decoration: const InputDecoration(
              border: InputBorder.none,
              hintText: 'input email',
              labelStyle: TextStyle(
                color: Colors.blueGrey,
              ),
            ),
            onChanged: (value) {},
          ),
        ),
        SizedBox(
          height: size.height * 0.020,
        ),
        Container(
          height: size.height * 0.065,
          width: size.width * 0.815,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: Colors.blueGrey,
              )),
          padding: EdgeInsets.only(left: 20, top: 1, right: 7),
          child: TextFormField(
            focusNode: _focusNodePass,
            keyboardType: TextInputType.text,
            controller: _passwordController,
            obscureText: _passwordVisible,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Enter your password',
              suffixIcon: IconButton(
                icon: Icon(
                  _passwordVisible
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                  color: Colors.black,
                ),
                onPressed: () {
                  setState(() {
                    _passwordVisible = !_passwordVisible;
                  });
                },
              ),
            ),
          ),
        ),
        SizedBox(
          height: size.height * 0.020,
        ),
        Container(
          height: size.height * 0.065,
          width: size.width * 0.815,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all(
                color: Colors.blueGrey,
              )),
          padding: EdgeInsets.only(left: 20, top: 1, right: 7),
          child: TextFormField(
            focusNode: _focusNodeConfirm,
            keyboardType: TextInputType.text,
            controller: _confirmController,
            obscureText: _confirmVisible,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: 'Enter your password',
              suffixIcon: IconButton(
                icon: Icon(
                  _confirmVisible
                      ? Icons.visibility_off_outlined
                      : Icons.visibility_outlined,
                  color: Colors.black,
                ),
                onPressed: () {
                  setState(() {
                    _confirmVisible = !_confirmVisible;
                  });
                },
              ),
            ),
          ),
        ),
        SizedBox(
          height: size.height * 0.066,
        ),
        Container(
            alignment: Alignment.center,
            child: Container(
                height: size.height * 0.055,
                width: size.width * 0.815,
                child: SignUpButton(onPressed: () async {
                  await provider.signUp(
                      email: _emailController.text,
                      username: _nameController.text,
                      password: _passwordController.text,
                      password_confirm: _confirmController.text);
                }))),
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "have an account?",
                style: const TextStyle(color: Colors.black, fontSize: 15),
              ),
              Login(onPressed: ((() {
                Navigator.pushReplacementNamed(context, LoginScreen.routeName);
              })))
            ],
          ),
        ),
        Spacer()
      ])),
    );
  }
}
